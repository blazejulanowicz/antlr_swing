package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {

	LocalSymbols ls = new LocalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void createGlobVar(String text) {
		ls.newSymbol(text);
	}
	
	protected void updateGlobVar(String text, Integer val) {
		ls.setSymbol(text, val);
	}
	
	protected Integer getGlobVar(String text) {
		return ls.getSymbol(text);
	}
	
	protected void enterScope() {
		ls.enterScope();
	}
	
	protected void leaveScope() {
		ls.leaveScope();
	}
	
	protected int add(int i1, int i2) {
		return i1+i2;
	}
	
	protected int sub(int i1, int i2) {
		return i1-i2;
	}
	
	protected int div(int i1, int i2) {
		if(i2 == 0) {
			throw new ArithmeticException("DZIELENIE PRZEZ ZERO JEST ZABRONIONE!!!!!!");
		}
		return i1/i2;
	}
	
	protected int mul(int i1, int i2) {
		return i1*i2;
	}
	
	protected int power(int i1, int i2) {
		return (int)Math.pow(i1, i2);
	}
	
	protected int mod(int i1, int i2) {
		return i1%i2;
	}
}
