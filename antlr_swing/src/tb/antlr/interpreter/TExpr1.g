tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : func*;
        
func    : (^(PRINT e=expr ) {drukuj ($e.text + " = " + $e.out.toString());}
        | method
        | expr
        ); 

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(MOD e1=expr e2=expr)   {$out = mod($e1.out, $e2.out);}
        | ^(POW e1=expr e2=expr)   {$out = power($e1.out, $e2.out);}
        | i1=ID                    {$out = getGlobVar($i1.text);}
        | INT                      {$out = getInt($INT.text);}
        ;

method
        : ^(PODST i1=ID   e2=expr) {updateGlobVar($i1.text, $e2.out);}
        | ^(VAR i1=ID) {createGlobVar($i1.text);}
        | ^(LB {enterScope();} func* {leaveScope();})
        ;