grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | bracketstat )+ EOF!;
    
bracketstat
    : LB^ (stat | bracketstat)* RB!
    ;

stat
    : expr NL -> expr

//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : power
      ( MUL^ power
      | DIV^ power
      | MOD^ power
      )*
    ;
    
power
    : atom
     (POW^ power
     )?;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

PRINT : 'print';

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

POW
  : '**'
  ;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD
  : '%'
  ;
  
LB
  : '{'
  ;
  
RB
  : '}'
  ;